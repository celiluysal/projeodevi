﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjeOdevi
{
    class Dortgen : Sekil
    {
        public Dortgen(AraSekil araSekil)
        {
            base.X = araSekil.X;
            base.Y = araSekil.Y;
            base.Genislik = araSekil.Genislik;
            base.Yukseklik = araSekil.Yukseklik;
            base.Renk = araSekil.Renk;
            base.sekilTipi = SekilTipi.dortgen;
            base.Secildi = false;
        }


        private string deneme;

        

        public override void Ciz(Graphics g)
        {
            SolidBrush dolgu = new SolidBrush(base.Renk);
            g.FillRectangle(dolgu, base.X, base.Y, base.Genislik, base.Yukseklik);
        }

        public override AraSekil BelirtecCiz(ref bool belirtec)
        {
            belirtec = true;
            return new AraSekil(base.X - 10, base.Y - 10, base.Genislik + 20, base.Yukseklik + 20, base.SecimRengi);
        }

        public override bool IcindeMi(Point n)
        {
            if ((base.X <= n.X && base.X + base.Genislik >= n.X) && (base.Y <= n.Y && base.Y + base.Yukseklik >= n.Y))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
