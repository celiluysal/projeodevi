﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjeOdevi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

                DoubleBuffered = true;
               // Paint += panel1_Paint;
        }

        private enum SekilSecimi { bos, dortgen, daire, ucgen, altigen }

        SekilSecimi sekilSecimi = new SekilSecimi();

        Color Renk = Color.Black;

        private int BaslangicX = 0;
        private int BaslangicY = 0;

        private List<Sekil> sekilListesi = new List<Sekil>();
        private int index;
        private int sayac;

        private Sekil sekil;
        private AraSekil dortgen;
        private AraSekil daire;
        private AraSekil ucgen;
        private AraSekil altigen;

        private Point baslangic;
        private Point Orta;
        private double yaricap;

        private bool secimDurumu = false;
        private bool tiklandi = false;
        private bool belirtec = false;
        private bool cizimDurumu = false;


        ////////////////////////////////////////////////////////////////////////
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {

            if (secimDurumu)
            {
                index = -1;
                sayac = 0;
                foreach (Sekil item in sekilListesi)
                {
                    tiklandi = true;
                    switch (item.sekilTipi)
                    {
                        case Sekil.SekilTipi.dortgen:
                            if (item.IcindeMi(new Point(e.X, e.Y)))
                            {
                                index = sayac;
                                dortgen = item.BelirtecCiz(ref belirtec);
                                sekil = new Dortgen(dortgen);   
                            }
                            break;
                        case Sekil.SekilTipi.daire:
                            if (item.IcindeMi(new Point(e.X, e.Y)))
                            {
                                index = sayac;
                                dortgen = item.BelirtecCiz(ref belirtec);
                                sekil = new Daire(dortgen);
                            }
                            break;
                        case Sekil.SekilTipi.ucgen:
                            if (item.IcindeMi(new Point(e.X, e.Y)))
                            {
                                index = sayac;
                                dortgen = item.BelirtecCiz(ref belirtec);
                                sekil = new Ucgen(dortgen);
                            }
                            break;
                        case Sekil.SekilTipi.altigen:
                            if (item.IcindeMi(new Point(e.X, e.Y)))
                            {
                                index = sayac;
                                dortgen = item.BelirtecCiz(ref belirtec);
                                sekil = new Altigen(dortgen);
                            }
                            break;
                        default:
                            break;
                    }
                    sayac++;
                    panel1.Invalidate();
                }
            }

            if (!cizimDurumu)
            {
                BaslangicX = e.X;
                BaslangicY = e.Y;
                baslangic = this.PointToClient(MousePosition);
            }

            switch (sekilSecimi)
            {
                case SekilSecimi.dortgen:
                    dortgen = new AraSekil(BaslangicX, BaslangicY, 0, 0, Renk);
                    sekil = new Dortgen(dortgen);
                    cizimDurumu = true;
                    break;
                case SekilSecimi.daire:
                    cizimDurumu = true;
                    break;
                case SekilSecimi.ucgen:
                    ucgen = new AraSekil(BaslangicX, BaslangicY, 0, 0, Renk);
                    sekil = new Ucgen(ucgen);
                    cizimDurumu = true;
                    break;
                case SekilSecimi.altigen:
                    altigen = new AraSekil(BaslangicX, BaslangicY, 0, 0, Renk);
                    sekil = new Altigen(altigen);
                    cizimDurumu = true;
                    break;
                default:
                    break;
            }
        }
        ////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (cizimDurumu)
            {
                Point pp = this.PointToClient(Cursor.Position);
                switch (sekilSecimi)
                {
                    case SekilSecimi.dortgen:
                        sekil.Genislik = e.X - BaslangicX;
                        sekil.Yukseklik = e.Y - BaslangicY;
                        panel1.Invalidate();
                        break;
                    case SekilSecimi.daire:
                        var p = Point.Subtract(baslangic, new Size(pp));
                        yaricap = Math.Sqrt(Math.Pow(p.X, 2) + Math.Pow(p.Y, 2));
                        Orta = Point.Subtract(baslangic, new Size((int)yaricap, (int)yaricap));
                        daire = new AraSekil(Orta.X, Orta.Y, 2 * (int)yaricap, 2 * (int)yaricap, Renk);
                        sekil = new Daire(daire);
                        panel1.Invalidate();
                        break;
                    case SekilSecimi.ucgen:
                        sekil.Genislik = (e.X - BaslangicX + e.Y - BaslangicY);
                        panel1.Invalidate();
                        break;
                    case SekilSecimi.altigen:
                        sekil.Genislik = (e.X - BaslangicX + e.Y - BaslangicY);
                        panel1.Invalidate();
                        break;
                    default:
                        break;
                }
            }
        }
        ////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            if (cizimDurumu)
            {
                sekilListesi.Add(sekil);
                baslangic = Point.Empty;
                cizimDurumu = false;
                panel1.Invalidate();
            }
        }
        ////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(panel1.BackColor);
            foreach (Sekil eleman in sekilListesi)
            {
                eleman.Ciz(e.Graphics);
            }

            if (cizimDurumu || belirtec)
            {
                sekil.Ciz(e.Graphics);
                belirtec = false;
            }
        }
        ////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////
        private void btnSec_Click(object sender, EventArgs e)
        {
            {
                btnDortgen.BackColor = Color.LightGray;
                btnDaire.BackColor = Color.LightGray;
                btnUcgen.BackColor = Color.LightGray;
                btnAltigen.BackColor = Color.LightGray;
                btnKaydet.BackColor = Color.LightGray;
                btnAc.BackColor = Color.LightGray;
                btnSil.BackColor = Color.LightGray;
                btnSec.BackColor = Color.Aqua;
            }
            secimDurumu = true;
            cizimDurumu = false;
            sekilSecimi = SekilSecimi.bos;

            
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            {
                btnDortgen.BackColor = Color.LightGray;
                btnDaire.BackColor = Color.LightGray;
                btnUcgen.BackColor = Color.LightGray;
                btnAltigen.BackColor = Color.LightGray;
                btnKaydet.BackColor = Color.LightGray;
                btnAc.BackColor = Color.LightGray;
                btnSil.BackColor = Color.Aqua;
                btnSec.BackColor = Color.LightGray;
            }
            if (secimDurumu && index > -1)
            {
                sekilListesi.RemoveAt(index);
                index = -1;
            }
            panel1.Invalidate();

            
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            {
                btnDortgen.BackColor = Color.LightGray;
                btnDaire.BackColor = Color.LightGray;
                btnUcgen.BackColor = Color.LightGray;
                btnAltigen.BackColor = Color.LightGray;
                btnKaydet.BackColor = Color.Aqua;
                btnAc.BackColor = Color.LightGray;
                btnSil.BackColor = Color.LightGray;
                btnSec.BackColor = Color.LightGray;
            }

            KayitEt.DosyaYaz(sekilListesi);  
        }

        private void btnAc_Click(object sender, EventArgs e)
        {
            {
                btnDortgen.BackColor = Color.LightGray;
                btnDaire.BackColor = Color.LightGray;
                btnUcgen.BackColor = Color.LightGray;
                btnAltigen.BackColor = Color.LightGray;
                btnKaydet.BackColor = Color.LightGray;
                btnAc.BackColor = Color.Aqua;
                btnSil.BackColor = Color.LightGray;
                btnSec.BackColor = Color.LightGray;
            }

            sekilListesi.Clear();
            sekilListesi.AddRange(KayitAc.DosyaOku());
            panel1.Invalidate();
        }
        ////////////////////////////////////////////////////////////////////////




        ////////////////////////////////////////////////////////////////////////
        private void btnDortgen_Click(object sender, EventArgs e)
        {
            sekilSecimi = SekilSecimi.dortgen;
            tiklandi = secimDurumu = false;

            {
                btnDortgen.BackColor = Color.Aqua;
                btnDaire.BackColor = Color.LightGray;
                btnUcgen.BackColor = Color.LightGray;
                btnAltigen.BackColor = Color.LightGray;
                btnKaydet.BackColor = Color.LightGray;
                btnAc.BackColor = Color.LightGray;
                btnSil.BackColor = Color.LightGray;
                btnSec.BackColor = Color.LightGray;
            }

        }

        private void btnDaire_Click(object sender, EventArgs e)
        {
            sekilSecimi = SekilSecimi.daire;
            tiklandi = secimDurumu = false;

            {
                btnDortgen.BackColor = Color.LightGray;
                btnDaire.BackColor = Color.Aqua;
                btnUcgen.BackColor = Color.LightGray;
                btnAltigen.BackColor = Color.LightGray;
                btnKaydet.BackColor = Color.LightGray;
                btnAc.BackColor = Color.LightGray;
                btnSil.BackColor = Color.LightGray;
                btnSec.BackColor = Color.LightGray;
            }
        }

        private void btnUcgen_Click(object sender, EventArgs e)
        {
            sekilSecimi = SekilSecimi.ucgen;
            tiklandi = secimDurumu = false;


            {
                btnDortgen.BackColor = Color.LightGray;
                btnDaire.BackColor = Color.LightGray;
                btnUcgen.BackColor = Color.Aqua;
                btnAltigen.BackColor = Color.LightGray;
                btnKaydet.BackColor = Color.LightGray;
                btnAc.BackColor = Color.LightGray;
                btnSil.BackColor = Color.LightGray;
                btnSec.BackColor = Color.LightGray;
            }
        }

        private void btnAltigen_Click(object sender, EventArgs e)
        {
            sekilSecimi = SekilSecimi.altigen;
            tiklandi = secimDurumu = false;

            {
                btnDortgen.BackColor = Color.LightGray;
                btnDaire.BackColor = Color.LightGray;
                btnUcgen.BackColor = Color.LightGray;
                btnAltigen.BackColor = Color.Aqua;
                btnKaydet.BackColor = Color.LightGray;
                btnAc.BackColor = Color.LightGray;
                btnSil.BackColor = Color.LightGray;
                btnSec.BackColor = Color.LightGray;
            }
        }
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////

        private void btnKirmizi_Click(object sender, EventArgs e)
        {
            if (!cizimDurumu)
                Renk = Color.Red;
            if (tiklandi)
                if (index != -1)
                    sekilListesi[index].Renk = Color.Red;
            panel1.Invalidate();

            {
                btnKirmizi.BackColor = Color.Aqua;
                btnMavi.BackColor = Color.LightGray;
                btnGri.BackColor = Color.LightGray;
                btnYesil.BackColor = Color.LightGray;
                btnSari.BackColor = Color.LightGray;
                btnSiyah.BackColor = Color.LightGray;
                btnTuruncu.BackColor = Color.LightGray;
                btnMor.BackColor = Color.LightGray;
                btnBeyaz.BackColor = Color.LightGray;
            }

        }

        private void btnMavi_Click(object sender, EventArgs e)
        {
            if (!cizimDurumu)
                Renk = Color.Blue;
            if (tiklandi)
                if (index != -1)
                    sekilListesi[index].Renk = Color.Blue;
            panel1.Invalidate();

            {
                btnKirmizi.BackColor = Color.LightGray;
                btnMavi.BackColor = Color.Aqua;
                btnGri.BackColor = Color.LightGray;
                btnYesil.BackColor = Color.LightGray;
                btnSari.BackColor = Color.LightGray;
                btnSiyah.BackColor = Color.LightGray;
                btnTuruncu.BackColor = Color.LightGray;
                btnMor.BackColor = Color.LightGray;
                btnBeyaz.BackColor = Color.LightGray;
            }
        }

        private void btnGri_Click(object sender, EventArgs e)
        {
            if (!cizimDurumu)
                Renk = Color.Gray;
            if (tiklandi)
                if (index != -1)
                    sekilListesi[index].Renk = Color.Gray;
            panel1.Invalidate();

            {
                btnKirmizi.BackColor = Color.LightGray;
                btnMavi.BackColor = Color.LightGray;
                btnGri.BackColor = Color.Aqua;
                btnYesil.BackColor = Color.LightGray;
                btnSari.BackColor = Color.LightGray;
                btnSiyah.BackColor = Color.LightGray;
                btnTuruncu.BackColor = Color.LightGray;
                btnMor.BackColor = Color.LightGray;
                btnBeyaz.BackColor = Color.LightGray;
            }
        }

        private void btnTuruncu_Click(object sender, EventArgs e)
        {
            if (!cizimDurumu)
                Renk = Color.Orange;
            if (tiklandi)
                if (index != -1)
                    sekilListesi[index].Renk = Color.Orange;
            panel1.Invalidate();

            {
                btnKirmizi.BackColor = Color.LightGray;
                btnMavi.BackColor = Color.LightGray;
                btnGri.BackColor = Color.LightGray;
                btnYesil.BackColor = Color.LightGray;
                btnSari.BackColor = Color.LightGray;
                btnSiyah.BackColor = Color.LightGray;
                btnTuruncu.BackColor = Color.Aqua;
                btnMor.BackColor = Color.LightGray;
                btnBeyaz.BackColor = Color.LightGray;
            }
        }

        private void btnSiyah_Click(object sender, EventArgs e)
        {
            if (!cizimDurumu)
                Renk = Color.Black;
            if (tiklandi)
                if (index != -1)
                    sekilListesi[index].Renk = Color.Black;
            panel1.Invalidate();

            {
                btnKirmizi.BackColor = Color.LightGray;
                btnMavi.BackColor = Color.LightGray;
                btnGri.BackColor = Color.LightGray;
                btnYesil.BackColor = Color.LightGray;
                btnSari.BackColor = Color.LightGray;
                btnSiyah.BackColor = Color.Aqua;
                btnTuruncu.BackColor = Color.LightGray;
                btnMor.BackColor = Color.LightGray;
                btnBeyaz.BackColor = Color.LightGray;
            }
        }

        private void btnSari_Click(object sender, EventArgs e)
        {
            if (!cizimDurumu)
                Renk = Color.Yellow;
            if (tiklandi)
                if (index != -1)
                    sekilListesi[index].Renk = Color.Yellow;
            panel1.Invalidate();

            {
                btnKirmizi.BackColor = Color.LightGray;
                btnMavi.BackColor = Color.LightGray;
                btnGri.BackColor = Color.LightGray;
                btnYesil.BackColor = Color.LightGray;
                btnSari.BackColor = Color.Aqua;
                btnSiyah.BackColor = Color.LightGray;
                btnTuruncu.BackColor = Color.LightGray;
                btnMor.BackColor = Color.LightGray;
                btnBeyaz.BackColor = Color.LightGray;
            }
        }

        private void btnMor_Click(object sender, EventArgs e)
        {
            if (!cizimDurumu)
                Renk = Color.Purple;
            if (tiklandi)
                if (index != -1)
                    sekilListesi[index].Renk = Color.Purple;
            panel1.Invalidate();

            {
                btnKirmizi.BackColor = Color.LightGray;
                btnMavi.BackColor = Color.LightGray;
                btnGri.BackColor = Color.LightGray;
                btnYesil.BackColor = Color.LightGray;
                btnSari.BackColor = Color.LightGray;
                btnSiyah.BackColor = Color.LightGray;
                btnTuruncu.BackColor = Color.LightGray;
                btnMor.BackColor = Color.Aqua;
                btnBeyaz.BackColor = Color.LightGray;
            }
        }

        private void btnYesil_Click(object sender, EventArgs e)
        {
            if (!cizimDurumu)
                Renk = Color.Green;
            if (tiklandi)
                if (index != -1)
                    sekilListesi[index].Renk = Color.Green;
            panel1.Invalidate();

            {
                btnKirmizi.BackColor = Color.LightGray;
                btnMavi.BackColor = Color.LightGray;
                btnGri.BackColor = Color.LightGray;
                btnYesil.BackColor = Color.Aqua;
                btnSari.BackColor = Color.LightGray;
                btnSiyah.BackColor = Color.LightGray;
                btnTuruncu.BackColor = Color.LightGray;
                btnMor.BackColor = Color.LightGray;
                btnBeyaz.BackColor = Color.LightGray;
            }
        }

        private void btnBeyaz_Click(object sender, EventArgs e)
        {

            if (!cizimDurumu)
                Renk = Color.White;
            if (tiklandi)
                if (index != -1)
                    sekilListesi[index].Renk = Color.White;
            panel1.Invalidate();

            {
                btnKirmizi.BackColor = Color.LightGray;
                btnMavi.BackColor = Color.LightGray;
                btnGri.BackColor = Color.LightGray;
                btnYesil.BackColor = Color.LightGray;
                btnSari.BackColor = Color.LightGray;
                btnSiyah.BackColor = Color.LightGray;
                btnTuruncu.BackColor = Color.LightGray;
                btnMor.BackColor = Color.LightGray;
                btnBeyaz.BackColor = Color.Aqua;
            }
        }



        ////////////////////////////////////////////////////////////////////////
    }
}
