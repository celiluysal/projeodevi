﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace ProjeOdevi
{
    class KayitAc
    {
        private static string satir;
        private static string[] kelimeler;

        private static Sekil sekil;
        private static AraSekil okunanSekil;

        private static List<Sekil> sekilListesi = new List<Sekil>();
        private static StreamReader dosyaOku;

        public static List<Sekil> DosyaOku()
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Metin Dosyası|*.txt";
            file.RestoreDirectory = true;
            file.CheckFileExists = false;
            file.Title = "Metin Dosyası (.txt) Seçiniz..";

            if (file.ShowDialog() == DialogResult.OK)
            {
                dosyaOku = File.OpenText(file.FileName);
                while ((satir = dosyaOku.ReadLine()) != null)
                {
                    kelimeler = satir.Split(' ');

                    okunanSekil = new AraSekil(Convert.ToInt32(kelimeler[0]), 
                        Convert.ToInt32(kelimeler[1]),
                        Convert.ToInt32(kelimeler[2]), 
                        Convert.ToInt32(kelimeler[3]),
                        Color.FromName(kelimeler[4]));

                    switch (kelimeler[5])
                    {
                        case "dortgen":
                            sekil = new Dortgen(okunanSekil);
                            break;
                        case "daire":
                            sekil = new Daire(okunanSekil);
                            break;
                        case "ucgen":
                            sekil = new Ucgen(okunanSekil);
                            break;
                        case "altigen":
                            sekil = new Altigen(okunanSekil);
                            break;
                        default:
                            break;
                    }
                    sekilListesi.Add(sekil);
                }
                dosyaOku.Close();
            }

            return sekilListesi;
        }
    }
}

