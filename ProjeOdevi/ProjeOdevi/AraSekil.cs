﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjeOdevi
{
    class AraSekil
    {
        public Color Renk;
        public int X;
        public int Y;
        public int Genislik;
        public int Yukseklik;

        public AraSekil(int X, int Y, int Genislik, int Yukseklik, Color Renk)
        {
            this.X = X;
            this.Y = Y;
            this.Genislik = Genislik;
            this.Yukseklik = Yukseklik;
            this.Renk = Renk;
        }
    }
}
