﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjeOdevi
{
    class Altigen : Sekil
    {
        public Altigen(AraSekil araSekil)
        {
            base.X = araSekil.X;
            base.Y = araSekil.Y;
            base.Genislik = araSekil.Genislik;
            base.Renk = araSekil.Renk;
            base.sekilTipi = SekilTipi.altigen;
        }

        private int dikme = 0;

        public override void Ciz(Graphics g)
        {
            SolidBrush dolgu = new SolidBrush(base.Renk);
            Point[] nokta = new Point[6];

            dikme = Convert.ToInt32((base.Genislik / 2) * 1.7);
            base.Yukseklik = dikme;

            //1
            nokta[0].X = X - Genislik / 2;
            nokta[0].Y = Y - dikme;

            //2
            nokta[1].X = X + Genislik / 2;
            nokta[1].Y = Y - dikme;

            //3
            nokta[2].X = X + Genislik;
            nokta[2].Y = Y;

            //4
            nokta[3].X = X + Genislik / 2;
            nokta[3].Y = Y + dikme;

            //5
            nokta[4].X = X - Genislik / 2;
            nokta[4].Y = Y + dikme;

            //6
            nokta[5].X = X - Genislik;
            nokta[5].Y = Y;

            g.FillPolygon(dolgu, nokta);
        }

        public override AraSekil BelirtecCiz(ref bool belirtec)
        {
            belirtec = true;
            return new AraSekil(base.X, base.Y, base.Genislik + 10, base.Yukseklik + 10, base.SecimRengi);
        }

        public override bool IcindeMi(Point n)
        {
            if ((base.X - base.Genislik <= n.X && base.X + base.Genislik >= n.X) && (base.Y - dikme <= n.Y && base.Y + dikme >= n.Y))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
