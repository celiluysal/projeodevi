﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjeOdevi
{
    class Ucgen : Sekil
    {
        public Ucgen(AraSekil araSekil)
        {
            base.X = araSekil.X;
            base.Y = araSekil.Y;
            Genislik = araSekil.Genislik;
            base.Renk = araSekil.Renk;
            base.sekilTipi = SekilTipi.ucgen;


            
        }

        private int dikme = 0;

        public override void Ciz(Graphics g)
        {
            SolidBrush dolgu = new SolidBrush(base.Renk);
            Point[] nokta = new Point[3];

            dikme = Convert.ToInt32((base.Genislik / 2) * 1.7);
            base.Yukseklik = dikme;

            //1
            nokta[0].X = base.X - base.Genislik / 2;
            nokta[0].Y = (dikme / 2) + base.Y;

            //2
            nokta[1].X = base.X + base.Genislik / 2;
            nokta[1].Y = (dikme / 2) + base.Y;

            //3
            nokta[2].X = base.X;
            nokta[2].Y = base.Y - (base.Genislik / 2);

            g.FillPolygon(dolgu, nokta);
        }

        public override AraSekil BelirtecCiz(ref bool belirtec)
        {
            belirtec = true;
            return new AraSekil(base.X, base.Y - 5 , base.Genislik + 30, 0, base.SecimRengi);
        }

        public override bool IcindeMi(Point n)
        {
            if (((base.X - base.Genislik/2 <= n.X) && (base.X + base.Genislik / 2 >= n.X) && (base.Y - dikme/2 <= n.Y) && (base.Y + dikme / 2 >= n.Y)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
