﻿namespace ProjeOdevi
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.gbCizimSekilleri = new System.Windows.Forms.GroupBox();
            this.btnAltigen = new System.Windows.Forms.Button();
            this.btnUcgen = new System.Windows.Forms.Button();
            this.btnDaire = new System.Windows.Forms.Button();
            this.btnDortgen = new System.Windows.Forms.Button();
            this.gbRenkler = new System.Windows.Forms.GroupBox();
            this.btnBeyaz = new System.Windows.Forms.Button();
            this.btnYesil = new System.Windows.Forms.Button();
            this.btnMor = new System.Windows.Forms.Button();
            this.btnSari = new System.Windows.Forms.Button();
            this.btnSiyah = new System.Windows.Forms.Button();
            this.btnTuruncu = new System.Windows.Forms.Button();
            this.btnGri = new System.Windows.Forms.Button();
            this.btnMavi = new System.Windows.Forms.Button();
            this.btnKirmizi = new System.Windows.Forms.Button();
            this.gbSekilİslemleri = new System.Windows.Forms.GroupBox();
            this.btnSil = new System.Windows.Forms.Button();
            this.btnSec = new System.Windows.Forms.Button();
            this.gbDosyaİslemleri = new System.Windows.Forms.GroupBox();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.btnAc = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gbCizimSekilleri.SuspendLayout();
            this.gbRenkler.SuspendLayout();
            this.gbSekilİslemleri.SuspendLayout();
            this.gbDosyaİslemleri.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCizimSekilleri
            // 
            this.gbCizimSekilleri.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCizimSekilleri.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gbCizimSekilleri.Controls.Add(this.btnAltigen);
            this.gbCizimSekilleri.Controls.Add(this.btnUcgen);
            this.gbCizimSekilleri.Controls.Add(this.btnDaire);
            this.gbCizimSekilleri.Controls.Add(this.btnDortgen);
            this.gbCizimSekilleri.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbCizimSekilleri.Location = new System.Drawing.Point(996, 12);
            this.gbCizimSekilleri.Name = "gbCizimSekilleri";
            this.gbCizimSekilleri.Size = new System.Drawing.Size(195, 195);
            this.gbCizimSekilleri.TabIndex = 2;
            this.gbCizimSekilleri.TabStop = false;
            this.gbCizimSekilleri.Text = "Çizim Şekilleri";
            // 
            // btnAltigen
            // 
            this.btnAltigen.BackColor = System.Drawing.Color.LightGray;
            this.btnAltigen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAltigen.BackgroundImage")));
            this.btnAltigen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAltigen.Location = new System.Drawing.Point(111, 110);
            this.btnAltigen.Name = "btnAltigen";
            this.btnAltigen.Size = new System.Drawing.Size(75, 75);
            this.btnAltigen.TabIndex = 3;
            this.btnAltigen.UseVisualStyleBackColor = false;
            this.btnAltigen.Click += new System.EventHandler(this.btnAltigen_Click);
            // 
            // btnUcgen
            // 
            this.btnUcgen.BackColor = System.Drawing.Color.LightGray;
            this.btnUcgen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUcgen.BackgroundImage")));
            this.btnUcgen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUcgen.Location = new System.Drawing.Point(16, 110);
            this.btnUcgen.Name = "btnUcgen";
            this.btnUcgen.Size = new System.Drawing.Size(75, 75);
            this.btnUcgen.TabIndex = 2;
            this.btnUcgen.UseVisualStyleBackColor = false;
            this.btnUcgen.Click += new System.EventHandler(this.btnUcgen_Click);
            // 
            // btnDaire
            // 
            this.btnDaire.BackColor = System.Drawing.Color.LightGray;
            this.btnDaire.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDaire.BackgroundImage")));
            this.btnDaire.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDaire.Location = new System.Drawing.Point(111, 29);
            this.btnDaire.Name = "btnDaire";
            this.btnDaire.Size = new System.Drawing.Size(75, 75);
            this.btnDaire.TabIndex = 1;
            this.btnDaire.UseVisualStyleBackColor = false;
            this.btnDaire.Click += new System.EventHandler(this.btnDaire_Click);
            // 
            // btnDortgen
            // 
            this.btnDortgen.BackColor = System.Drawing.Color.LightGray;
            this.btnDortgen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDortgen.BackgroundImage")));
            this.btnDortgen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDortgen.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDortgen.Location = new System.Drawing.Point(16, 29);
            this.btnDortgen.Name = "btnDortgen";
            this.btnDortgen.Size = new System.Drawing.Size(75, 75);
            this.btnDortgen.TabIndex = 0;
            this.btnDortgen.UseVisualStyleBackColor = false;
            this.btnDortgen.Click += new System.EventHandler(this.btnDortgen_Click);
            // 
            // gbRenkler
            // 
            this.gbRenkler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbRenkler.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gbRenkler.Controls.Add(this.btnBeyaz);
            this.gbRenkler.Controls.Add(this.btnYesil);
            this.gbRenkler.Controls.Add(this.btnMor);
            this.gbRenkler.Controls.Add(this.btnSari);
            this.gbRenkler.Controls.Add(this.btnSiyah);
            this.gbRenkler.Controls.Add(this.btnTuruncu);
            this.gbRenkler.Controls.Add(this.btnGri);
            this.gbRenkler.Controls.Add(this.btnMavi);
            this.gbRenkler.Controls.Add(this.btnKirmizi);
            this.gbRenkler.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbRenkler.Location = new System.Drawing.Point(996, 213);
            this.gbRenkler.Name = "gbRenkler";
            this.gbRenkler.Size = new System.Drawing.Size(195, 219);
            this.gbRenkler.TabIndex = 3;
            this.gbRenkler.TabStop = false;
            this.gbRenkler.Text = "Renk Seçimi";
            // 
            // btnBeyaz
            // 
            this.btnBeyaz.BackColor = System.Drawing.Color.LightGray;
            this.btnBeyaz.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBeyaz.BackgroundImage")));
            this.btnBeyaz.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBeyaz.Location = new System.Drawing.Point(134, 157);
            this.btnBeyaz.Name = "btnBeyaz";
            this.btnBeyaz.Size = new System.Drawing.Size(55, 55);
            this.btnBeyaz.TabIndex = 12;
            this.btnBeyaz.UseVisualStyleBackColor = false;
            this.btnBeyaz.Click += new System.EventHandler(this.btnBeyaz_Click);
            // 
            // btnYesil
            // 
            this.btnYesil.BackColor = System.Drawing.Color.LightGray;
            this.btnYesil.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnYesil.BackgroundImage")));
            this.btnYesil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnYesil.Location = new System.Drawing.Point(70, 157);
            this.btnYesil.Name = "btnYesil";
            this.btnYesil.Size = new System.Drawing.Size(55, 55);
            this.btnYesil.TabIndex = 11;
            this.btnYesil.UseVisualStyleBackColor = false;
            this.btnYesil.Click += new System.EventHandler(this.btnYesil_Click);
            // 
            // btnMor
            // 
            this.btnMor.BackColor = System.Drawing.Color.LightGray;
            this.btnMor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMor.BackgroundImage")));
            this.btnMor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMor.Location = new System.Drawing.Point(6, 157);
            this.btnMor.Name = "btnMor";
            this.btnMor.Size = new System.Drawing.Size(55, 55);
            this.btnMor.TabIndex = 10;
            this.btnMor.UseVisualStyleBackColor = false;
            this.btnMor.Click += new System.EventHandler(this.btnMor_Click);
            // 
            // btnSari
            // 
            this.btnSari.BackColor = System.Drawing.Color.LightGray;
            this.btnSari.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSari.BackgroundImage")));
            this.btnSari.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSari.Location = new System.Drawing.Point(132, 93);
            this.btnSari.Name = "btnSari";
            this.btnSari.Size = new System.Drawing.Size(55, 55);
            this.btnSari.TabIndex = 9;
            this.btnSari.UseVisualStyleBackColor = false;
            this.btnSari.Click += new System.EventHandler(this.btnSari_Click);
            // 
            // btnSiyah
            // 
            this.btnSiyah.BackColor = System.Drawing.Color.Aqua;
            this.btnSiyah.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSiyah.BackgroundImage")));
            this.btnSiyah.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSiyah.Location = new System.Drawing.Point(69, 93);
            this.btnSiyah.Name = "btnSiyah";
            this.btnSiyah.Size = new System.Drawing.Size(55, 55);
            this.btnSiyah.TabIndex = 8;
            this.btnSiyah.UseVisualStyleBackColor = false;
            this.btnSiyah.Click += new System.EventHandler(this.btnSiyah_Click);
            // 
            // btnTuruncu
            // 
            this.btnTuruncu.BackColor = System.Drawing.Color.LightGray;
            this.btnTuruncu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTuruncu.BackgroundImage")));
            this.btnTuruncu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTuruncu.ForeColor = System.Drawing.SystemColors.Control;
            this.btnTuruncu.Location = new System.Drawing.Point(6, 93);
            this.btnTuruncu.Name = "btnTuruncu";
            this.btnTuruncu.Size = new System.Drawing.Size(55, 55);
            this.btnTuruncu.TabIndex = 7;
            this.btnTuruncu.UseVisualStyleBackColor = false;
            this.btnTuruncu.Click += new System.EventHandler(this.btnTuruncu_Click);
            // 
            // btnGri
            // 
            this.btnGri.BackColor = System.Drawing.Color.LightGray;
            this.btnGri.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGri.BackgroundImage")));
            this.btnGri.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGri.Location = new System.Drawing.Point(134, 29);
            this.btnGri.Name = "btnGri";
            this.btnGri.Size = new System.Drawing.Size(55, 55);
            this.btnGri.TabIndex = 6;
            this.btnGri.UseVisualStyleBackColor = false;
            this.btnGri.Click += new System.EventHandler(this.btnGri_Click);
            // 
            // btnMavi
            // 
            this.btnMavi.BackColor = System.Drawing.Color.LightGray;
            this.btnMavi.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMavi.BackgroundImage")));
            this.btnMavi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMavi.Location = new System.Drawing.Point(70, 29);
            this.btnMavi.Name = "btnMavi";
            this.btnMavi.Size = new System.Drawing.Size(55, 55);
            this.btnMavi.TabIndex = 5;
            this.btnMavi.UseVisualStyleBackColor = false;
            this.btnMavi.Click += new System.EventHandler(this.btnMavi_Click);
            // 
            // btnKirmizi
            // 
            this.btnKirmizi.BackColor = System.Drawing.Color.LightGray;
            this.btnKirmizi.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnKirmizi.BackgroundImage")));
            this.btnKirmizi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnKirmizi.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnKirmizi.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnKirmizi.Location = new System.Drawing.Point(6, 29);
            this.btnKirmizi.Name = "btnKirmizi";
            this.btnKirmizi.Size = new System.Drawing.Size(55, 55);
            this.btnKirmizi.TabIndex = 4;
            this.btnKirmizi.TabStop = false;
            this.btnKirmizi.UseVisualStyleBackColor = false;
            this.btnKirmizi.Click += new System.EventHandler(this.btnKirmizi_Click);
            // 
            // gbSekilİslemleri
            // 
            this.gbSekilİslemleri.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSekilİslemleri.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gbSekilİslemleri.Controls.Add(this.btnSil);
            this.gbSekilİslemleri.Controls.Add(this.btnSec);
            this.gbSekilİslemleri.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbSekilİslemleri.Location = new System.Drawing.Point(996, 438);
            this.gbSekilİslemleri.Name = "gbSekilİslemleri";
            this.gbSekilİslemleri.Size = new System.Drawing.Size(195, 118);
            this.gbSekilİslemleri.TabIndex = 4;
            this.gbSekilİslemleri.TabStop = false;
            this.gbSekilİslemleri.Text = "Şekil İşlemleri";
            // 
            // btnSil
            // 
            this.btnSil.BackColor = System.Drawing.Color.LightGray;
            this.btnSil.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSil.BackgroundImage")));
            this.btnSil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSil.FlatAppearance.BorderSize = 0;
            this.btnSil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSil.Location = new System.Drawing.Point(106, 29);
            this.btnSil.Name = "btnSil";
            this.btnSil.Size = new System.Drawing.Size(83, 75);
            this.btnSil.TabIndex = 1;
            this.btnSil.UseVisualStyleBackColor = false;
            this.btnSil.Click += new System.EventHandler(this.btnSil_Click);
            // 
            // btnSec
            // 
            this.btnSec.BackColor = System.Drawing.Color.LightGray;
            this.btnSec.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSec.BackgroundImage")));
            this.btnSec.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSec.FlatAppearance.BorderSize = 0;
            this.btnSec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSec.Location = new System.Drawing.Point(16, 29);
            this.btnSec.Name = "btnSec";
            this.btnSec.Size = new System.Drawing.Size(80, 75);
            this.btnSec.TabIndex = 0;
            this.btnSec.UseVisualStyleBackColor = false;
            this.btnSec.Click += new System.EventHandler(this.btnSec_Click);
            // 
            // gbDosyaİslemleri
            // 
            this.gbDosyaİslemleri.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDosyaİslemleri.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gbDosyaİslemleri.Controls.Add(this.btnKaydet);
            this.gbDosyaİslemleri.Controls.Add(this.btnAc);
            this.gbDosyaİslemleri.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbDosyaİslemleri.Location = new System.Drawing.Point(995, 562);
            this.gbDosyaİslemleri.Name = "gbDosyaİslemleri";
            this.gbDosyaİslemleri.Size = new System.Drawing.Size(195, 118);
            this.gbDosyaİslemleri.TabIndex = 5;
            this.gbDosyaİslemleri.TabStop = false;
            this.gbDosyaİslemleri.Text = "Dosya İşlemleri";
            // 
            // btnKaydet
            // 
            this.btnKaydet.BackColor = System.Drawing.Color.LightGray;
            this.btnKaydet.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnKaydet.BackgroundImage")));
            this.btnKaydet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnKaydet.FlatAppearance.BorderSize = 0;
            this.btnKaydet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKaydet.Location = new System.Drawing.Point(106, 29);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Size = new System.Drawing.Size(83, 75);
            this.btnKaydet.TabIndex = 1;
            this.btnKaydet.UseVisualStyleBackColor = false;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // btnAc
            // 
            this.btnAc.BackColor = System.Drawing.Color.LightGray;
            this.btnAc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAc.BackgroundImage")));
            this.btnAc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAc.FlatAppearance.BorderSize = 0;
            this.btnAc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAc.Location = new System.Drawing.Point(16, 29);
            this.btnAc.Name = "btnAc";
            this.btnAc.Size = new System.Drawing.Size(83, 75);
            this.btnAc.TabIndex = 0;
            this.btnAc.UseVisualStyleBackColor = false;
            this.btnAc.Click += new System.EventHandler(this.btnAc_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(12, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(978, 664);
            this.panel1.TabIndex = 6;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1202, 692);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gbDosyaİslemleri);
            this.Controls.Add(this.gbSekilİslemleri);
            this.Controls.Add(this.gbRenkler);
            this.Controls.Add(this.gbCizimSekilleri);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbCizimSekilleri.ResumeLayout(false);
            this.gbRenkler.ResumeLayout(false);
            this.gbSekilİslemleri.ResumeLayout(false);
            this.gbDosyaİslemleri.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbCizimSekilleri;
        private System.Windows.Forms.GroupBox gbRenkler;
        private System.Windows.Forms.Button btnAltigen;
        private System.Windows.Forms.Button btnUcgen;
        private System.Windows.Forms.Button btnDaire;
        private System.Windows.Forms.Button btnDortgen;
        private System.Windows.Forms.Button btnKirmizi;
        private System.Windows.Forms.Button btnGri;
        private System.Windows.Forms.Button btnMavi;
        private System.Windows.Forms.Button btnBeyaz;
        private System.Windows.Forms.Button btnYesil;
        private System.Windows.Forms.Button btnMor;
        private System.Windows.Forms.Button btnSari;
        private System.Windows.Forms.Button btnSiyah;
        private System.Windows.Forms.Button btnTuruncu;
        private System.Windows.Forms.GroupBox gbSekilİslemleri;
        private System.Windows.Forms.Button btnSil;
        private System.Windows.Forms.Button btnSec;
        private System.Windows.Forms.GroupBox gbDosyaİslemleri;
        private System.Windows.Forms.Button btnKaydet;
        private System.Windows.Forms.Button btnAc;
        private System.Windows.Forms.Panel panel1;
    }
}

