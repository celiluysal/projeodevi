﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjeOdevi
{
    abstract class Sekil
    {
        public Color Renk;
        public int X;
        public int Y;
        public int Genislik;
        public int Yukseklik;
        public bool Secildi;


        public Color SecimRengi = Color.FromArgb(60, Color.Aqua);

        public enum SekilTipi
        {
            dortgen, daire, ucgen, altigen
        }

        public SekilTipi sekilTipi { get; set; }

        public abstract void Ciz(Graphics g);

        public abstract AraSekil BelirtecCiz(ref bool belirtec);

        public abstract bool IcindeMi(Point n);

    }
}
