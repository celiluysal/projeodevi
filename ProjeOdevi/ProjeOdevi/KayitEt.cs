﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace ProjeOdevi
{
    class KayitEt
    {
        private static string[] kelime;
        private static string[] karakterler = new string[] {" ", "Color", "[", "]" };

        public static void DosyaYaz(List<Sekil> _sekilListesi)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Metin Dosyası|*.txt";
            save.OverwritePrompt = true;
            save.CreatePrompt = true;

            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter dosyaYaz = new StreamWriter(save.FileName);
                foreach (Sekil item in _sekilListesi)
                {
                    kelime = item.Renk.ToString().Split(karakterler, StringSplitOptions.RemoveEmptyEntries);
                    dosyaYaz.WriteLine(item.X + " " + item.Y + " " + item.Genislik + " " + item.Yukseklik + " " + kelime[0] + " " + item.sekilTipi.ToString());
                }
                dosyaYaz.Close();
            }
        }
    }
}
